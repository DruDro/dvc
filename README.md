# Gulp Front


## Quick Start

* Install [node.js](https://nodejs.org)

* Install [git](https://git-for-windows.github.io/)

* For [NPM](https://www.npmjs.com)
	- Update npm to the latest version
		Go to the folder with installed nodejs

		```bash
		cd "C:\Program Files\nodejs"
		```

		or

		```bash
		cd "C:\Program Files (x86)\nodejs"
		```

		and run

		```bash
		npm install npm@latest
		```

	- Install `gulp` globally (once!)

		```bash
		npm install gulp -g
		```
	- Install `bower` globally (once!)

		```bash
		npm install -g bower
		```

	- Install dependencies (once for a project)

		```bash
		npm install
		```

		```bash
		bower install
		```

* Run `gulp`

	```bash
	gulp develop
	```

* Browser will open the running project at [`http://localhost:3000/`](http://localhost:3000/)


## Main gulp tasks

* `gulp develop`      - runs watchers and server and distributes to `build` 
* `gulp sprite:retina`     - builds retina sprite
* `gulp sprite:svg`        - builds svg sprite
* `gulp sprite:default`    - builds default sprite
* `gulp production`   - builds the project and distributes to `build` 

