$(function () {
	(function setBoardBg() {
		var board = $('.board');
		board.each(function(){
			var boardBg = $(this).data("bg");
			$(this).css("background-image", "url(" + boardBg + ")");
		});
	})();

	function fillSpace() {
		var wh = $(window).height(),
			dh = $('body').height(),
			diff = wh - dh,
			board = $('.board'),
			bh = board.outerHeight(),
			page = $('.page'),
			ph = page.outerHeight();
		if (diff > 0) {
			if (board.length) {
				board.css("min-height", bh + diff);
			}
			else {
				page.css("min-height", ph + diff);
			}
		}
	};
	$(window).on("resize load", fillSpace);


	//carousel
	$('.home__board-carousel').slick(
		{
			autoplay:true,
			autoplaySpeed:5000,
			speed: 500,
			infinite:true,
			dots: true,
			arrows: true,
			appendDots: '.home__board-carousel',
			prevArrow: '<span type="button" class="slick-prev"></span>',
			nextArrow: '<span type="button" class="slick-next"></span>',
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false
					}
				}
			]
		}
	);

	$('.home__board-carousel').on('breakpoint', function (event, slick, breakpoint) {
		fillSpace();
	});
});