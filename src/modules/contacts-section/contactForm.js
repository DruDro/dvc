
$(function() {

    var $form = $('#contactForm');



    //AJAX
    var request;

    $form.submit(function(event) {

        event.preventDefault();
            if (request) {
                request.abort();
            }
            var $inputs = $form.find("input, textarea, button");

            var serializedData = $form.serialize();

            var username = $('#name').val();
            var phone = $('#phone').val();

            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "contactForm.php",
                type: "post",
                data: serializedData
            });

            // Callback handler that will be called on success
            request.done(function(response, textStatus, jqXHR) {
                // Log a message to the console
				$form.addClass('sent');
				console.log(response, textStatus, jqXHR)
            });

            // Callback handler that will be called on failure
            request.fail(function(jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.error(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );

                $form.addClass('fail');
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function() {
                // Reenable the inputs
                $inputs.prop("disabled", true).css("cursor","not-allowed");
            });
            return false;
    });

    
});
