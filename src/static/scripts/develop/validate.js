;(function() { 
	
	var $form = $('form');
	
	if ($form.length) {
		$form.each(function() {
			$(this).validate({
				errorPlacement: function(error, element) {
					element.attr("placeholder",error.text());
				},
				ignore: [],
				rules: {
					agree: {
						required: true
					}
				}
			});
		});
	}
	
	jQuery.extend(jQuery.validator.messages, {
    	required: "Поле не заповнене",
	});
	
}($));