//---------------------------------------//
// Подключаем скрипты компонентов страниц ( modules )
//---------------------------------------//


//---------------------------------------//
// Подключаем основные скрипты ( develop )
//---------------------------------------//

//= require develop/tabs.js
//= require develop/dropdowns.js
//= require develop/popups.js
//= require develop/fluidVideo.js
//= require develop/scrollAnchor.js

//= require develop/main.js

//= require ../../modules/**/*.js